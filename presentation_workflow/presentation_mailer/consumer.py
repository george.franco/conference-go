import pika
from pika.exceptions import AMQPConnectionError
import json
import django
import os
import sys
from django.core.mail import send_mail
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()

while True:
    try:

        def process_approval(ch, method, properties, body):
            presentation = json.loads(body)
            send_mail(
                "Your presentation has been accepted",
                f"{presentation['presenter_name']}, we're happy to tell you that your presentation {presentation['title']} has been accepted",
                "admin@conference.go",
                [presentation["presenter_email"]],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            presentation = json.loads(body)
            send_mail(
                "Your presentation has been rejected",
                f"{presentation['presenter_name']}, we are sad to let you know that presentation {presentation['title']} has bee rejected",
                "admin@conference.go",
                [presentation["presenter_email"]],
                fail_silently=False,
            )

        queue_dict = {
            "presentations_approvals": process_approval,
            "presentations_rejections": process_rejection,
        }

        for queue_name, func in queue_dict.items():
            channel.queue_declare(queue=queue_name)
            channel.basic_consume(
                queue=queue_name,
                on_message_callback=func,
                auto_ack=True,
            )

        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
